<?php
declare(strict_types=1);

require_once('config.php');

$GLOBALS['pdo'] = NULL;

function getPDO():PDO
{
	if ($GLOBALS['pdo'] == NULL)
	{
		$dsn = 'sqlite:'.DB_FILE;
		$GLOBALS['pdo'] = new PDO($dsn);
	}
	return $GLOBALS['pdo'];
}

function createDbIfRequired():void
{
	if (file_exists(DB_FILE))
	{
		return;
	}

	$pdo = getPDO();
	
	$pdo->exec(<<<SQL
CREATE TABLE
vault
(
	id INTEGER PRIMARY KEY AUTOINCREMENT,
	path TEXT NOT NULL,
	content TEXT NOT NULL
)
;
SQL
	);
}

function respond(int $statusCode, array $headers, string $text):void
{
	http_response_code($statusCode);
	header('Access-Control-Allow-Origin: *');
	foreach ($headers as $header)
	{
		header($header);
	}
	echo $text;
	exit;
}

function respondJson(int $statusCode, array $response):void
{
	$headers = [
		'Content-Type: application/json'
	];

	$text = json_encode($response);

	respond($statusCode, $headers, $text);
}

function respondError(int $statusCode, string $errorMessage):void
{
	$response = ['error' => $errorMessage];
	respondJson($statusCode, $response);
}

function respondRequestError(string $text):void
{
	respondError(400, $text);
}

function selectIdByPath(string $path)
{
	$pdo = getPDO();

	$statement = $pdo->prepare(<<<SQL
SELECT
	id
FROM
	vault
WHERE
	path = :path
;
SQL
	);

	$statement->execute([
		':path' => $path
	]);

	$result = $statement->fetch(PDO::FETCH_ASSOC);

	if ($result === false)
	{
		return [];
	}
	return $result;
}

function selectContentByPath(string $path):array
{
	$pdo = getPDO();

	$statement = $pdo->prepare(<<<SQL
SELECT
	content
FROM
	vault
WHERE
	path = :path
;
SQL
	);

	$statement->execute([
		':path' => $path
	]);

	$result = $statement->fetch(PDO::FETCH_ASSOC);

	if ($result === false)
	{
		return [];
	}
	return $result;
}

function insert(string $path, string $content):bool
{
	$pdo = getPDO();

	$statement = $pdo->prepare(<<<SQL
INSERT INTO
	vault
	(
		path,
		content
	)
	VALUES
	(
		:path,
		:content
	)
;
SQL
	);

	return $statement->execute([
		':path' => $path,
		':content' => $content,
	]);
}

function update(int $id, string $content)
{
	$pdo = getPDO();

	$statement = $pdo->prepare(<<<SQL
UPDATE
	vault
SET
	content = :content
WHERE
	id = :id
;
SQL
	);

	return $statement->execute([
		':id' => $id,
		':content' => $content,
	]);
}

function insertOrUpdate(string $path, string $content):bool
{
	$result = selectIdByPath($path);

	return
		isset($result['id'])
			? update((int) $result['id'], $content)
			: insert($path, $content)
	;
}

function buildPath():string
{
	$path = $_SERVER['REQUEST_URI'];

	if (substr($path, 0, 1) === '/')
	{
		$path = substr($path, 1);
	}

	if (substr($path, -1) === '/')
	{
		$path = substr($path, 0, -1);
	}

	if (strpos($path, CLOUD_TEXT_BASE_PATH) === 0)
	{
		$path = substr($path, strlen(CLOUD_TEXT_BASE_PATH));
	}

	if (substr($path, 0, 1) === '/')
	{
		$path = substr($path, 1);
	}

	return $path;
}

function actionPost():void
{
	$path = buildPath();
	$body = file_get_contents('php://input');
	$result = insertOrUpdate($path, $body);
	if ($result)
	{
		respondJson(200, ['result' => 'OK']);
	}
	respondError(500, 'Something went wrong processing the request.');
}

function actionGet():void
{
	$path = buildPath();
	$result = selectContentByPath($path);
	if (isset($result['content']))
	{
		respond(200, [], $result['content']);
	}
	respondError(404, 'Content not found');
}

function run():void
{
	switch ($_SERVER['REQUEST_METHOD'])
	{
		case 'POST':
			actionPost();
			break;
		case 'GET':
			actionGet();
			break;
		default:
			respondRequestError('Invalid HTTP Request Method');
	}
	respondError(500, 'Internal server error.');
}



createDbIfRequired();

run();